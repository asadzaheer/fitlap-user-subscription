<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class ApiController extends Controller {
    function generateMenu($userId) {
        if (!$userId) {
            return Response::json(array(
                'code' => 400,
                'message' => 'Parameter missing.'
            ), 400);
        }

        if (!DB::table('users')->find($userId)) {
            return Response::json(array(
                'code' => 404,
                'message' => 'User does not exist.'
            ), 404);
        }

        $product = DB::table('am_access')->where('user_id', $userId)
            ->where('product_id', '!=', 6)
            ->where('begin_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('expire_date', '>=', Carbon::now()->format('Y-m-d'))->first();

        if ($product) {
            return Response::json(array(
                'code' => 200,
                'message' => 'Menu generated successfully.',
                'menu' => ['data' => 'some menu data']
            ), 200);
        } else {
            return Response::json(array(
                'code' => 204,
                'message' => 'User does not have active subscription.'
            ), 204);
        }
    }
}
