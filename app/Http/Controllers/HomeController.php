<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller {
    public function index() {
        $product = DB::table('am_access')->where('user_id', Auth::user()->id)
            ->where('product_id', '!=', 6)
            ->where('begin_date', '<=', Carbon::now()->format('Y-m-d'))
            ->where('expire_date', '>=', Carbon::now()->format('Y-m-d'))->first();

        return view('home', ['user' => Auth::user(), 'product' => $product]);
    }
}
