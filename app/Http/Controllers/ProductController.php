<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller {
    public function index() {
        return view('addProduct');
    }

    public function store(Request $request) {
        $expireDate = Carbon::now();
        switch ($request->input('sub')) {
            case '1m':
                $expireDate = $expireDate->addMonth();
                break;
            case '1y':
                $expireDate = $expireDate->addYear();
                break;
            case 'lt':
                $expireDate = Carbon::create(2037, 12, 31, 0);;
                break;
        }

        $product = ['user_id' => Auth::user()->id, 'product_id' => $request->input('product'), 'begin_date' => Carbon::now(), 'expire_date' => $expireDate];

        DB::table('am_access')->insert($product);
        return redirect('home');
    }
}
