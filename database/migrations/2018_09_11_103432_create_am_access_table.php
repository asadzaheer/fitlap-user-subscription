<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmAccessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('am_access', function (Blueprint $table) {
            $table->increments('	access_id');
            $table->integer('invoice_id')->nullable();
            $table->integer('user_id');
            $table->integer('product_id');
            $table->string('transaction_id')->nullable();
            $table->date('begin_date');
            $table->date('expire_date');
            $table->integer('qty');
            $table->string('comment');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('am_access');
    }
}
