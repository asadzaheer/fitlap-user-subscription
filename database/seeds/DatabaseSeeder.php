<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert(
            ['name' => 'user1', 'email' => 'user1@test.com', 'password' => bcrypt('123')],
            ['name' => 'user2', 'email' => 'user2@test.com', 'password' => bcrypt('123')],
            ['name' => 'user3', 'email' => 'user3@test.com', 'password' => bcrypt('123'),],
            ['name' => 'user4', 'email' => 'user4@test.com', 'password' => bcrypt('123'),],
            ['name' => 'user5', 'email' => 'user5@test.com', 'password' => bcrypt('123'),]
        );
        DB::table('am_access')->insert(
            ['invoice_id' => null, 'user_id' => 1,
                'product_id' => 4, 'transaction_id' => null,
                'begin_date' =>  Carbon::create('2017', '11', '21'),
                'expire_date' => Carbon::create('2017', '11', '21'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 1,
                'product_id' => 6, 'transaction_id' => null,
                'begin_date' => Carbon::create('2018', '03', '20'),
                'expire_date' => Carbon::create('2037', '12', '31'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => 11, 'user_id' => 1,
                'product_id' => 7, 'transaction_id' => null,
                'begin_date' => Carbon::create('2017', '12', '12'),
                'expire_date' => Carbon::create('2017', '12', '26'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 1,
                'product_id' => 1, 'transaction_id' => null,
                'begin_date' => Carbon::create('2018', '09', '11'),
                'expire_date' => Carbon::create('2018', '10', '11'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 2,
                'product_id' => 2, 'transaction_id' => null,
                'begin_date' => Carbon::create('2017', '11', '30'),
                'expire_date' => Carbon::create('2017', '12', '01'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 2,
                'product_id' => 2, 'transaction_id' => null,
                'begin_date' => Carbon::create('2017', '11', '24'),
                'expire_date' => Carbon::create('2017', '11', '25'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => 2, 'user_id' => 2,
                'product_id' => 4, 'transaction_id' => null,
                'begin_date' => Carbon::create('2017', '11', '21'),
                'expire_date' => Carbon::create('2017', '11', '24'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 3,
                'product_id' => 6, 'transaction_id' => null,
                'begin_date' => Carbon::create('2017', '12', '07'),
                'expire_date' => Carbon::create('2037', '12', '31'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 3,
                'product_id' => 6, 'transaction_id' => null,
                'begin_date' => Carbon::create('2018', '03', '20'),
                'expire_date' => Carbon::create('2037', '12', '31'),
                'qty' => 1, 'comment' => ''],
            ['invoice_id' => null, 'user_id' => 4,
                'product_id' => 6, 'transaction_id' => null,
                'begin_date' => Carbon::create('2018', '03', '20'),
                'expire_date' => Carbon::create('2037', '12', '31'),
                'qty' => 1, 'comment' => '']
        );
    }
}
