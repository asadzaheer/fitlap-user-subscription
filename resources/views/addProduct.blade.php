@extends('app')

@section('title', 'Home')

@section('content')

<div class="row justify-content-md-center" style="height: 100vh;">
    <div class="col-sm-6" style="margin-top: auto; margin-bottom: auto;">
        <h3>Add Product </h3>
        <form method="post" action="{{ route('addProduct') }}">
            <div class="form-group">
                @csrf
                <label for="product">Product:</label>
                <select class="form-control" name="product">
                    <option value="1">Product 1</option>
                    <option value="2">Product 2</option>
                    <option value="3">Product 3</option>
                    <option value="4">Product 4</option>
                </select>
            </div>
            <div class="form-group">
                <label for="sub">Subscription Time:</label>
                <select class="form-control" name="sub">
                    <option value="1m">1 Month</option>
                    <option value="1y">1 Year</option>
                    <option value="lt">Life Time</option>
                </select>
            </div>
            <button type="submit" class="btn btn-primary">Add</button>
        </form>
    </div>
</div>
@endsection
