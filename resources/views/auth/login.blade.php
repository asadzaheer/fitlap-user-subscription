@extends('app')

@section('title', 'Login')

@section('content')
<div class="row justify-content-md-center" style="height: 100vh;">
    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    <form method="post" class="col-sm-6" action="{{ route('login') }}" style="margin-top: auto; margin-bottom: auto;">
        <p class="h3">Welcome to Fitlap Subscription Task</p>
        <div class="form-group">
            @csrf
            <label for="email">Email:</label>
            <input type="email" class="form-control" name="email"/>
        </div>
        <div class="form-group">
            <label for="password">Password :</label>
            <input type="password" class="form-control" name="password"/>
        </div>
        <button type="submit" class="btn btn-primary">Login</button>
    </form>
</div>
@endsection
