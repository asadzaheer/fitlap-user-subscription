@extends('app')

@section('title', 'Home')

@section('content')

<div class="row justify-content-md-center" style="height: 100vh;">
    <div class="col-sm-12" style="margin-top: auto; margin-bottom: auto;">
        <h1>Welcome </h1>
        <p>{{$user->name}} <a href="{{route('logout')}}" class="btn btn-sm btn-danger">Logout</a></p>
        <a href="{{route('addProduct')}}" class="btn btn-sm btn-primary">Add Product</a>
        @if($product)
        <p>Your products</p>
        <div class="row justify-content-md-center">
            <div class="col-sm-4 border border-primary">
                <div>Product : {{$product->product_id}}</div>
                <div>Status : {{($product->product_id != 6)?'Active':'Inactive'}}</div>
                @if (Carbon\Carbon::parse($product->expire_date)->eq(Carbon\Carbon::parse('2037-12-31')))
                <div>Expires : Never</div>
                @else
                <div>Expires : {{$product->expire_date}}</div>
                @endif
            </div>
        </div>
        @else
        <div class="row justify-content-md-center">
            <p class="col-sm-4 border border-primary">You don't have access to any product.</p>
        </div>
        @endif
    </div>
</div>
@endsection
