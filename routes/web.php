<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
Route::get('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);

Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index'])->middleware('auth');

Route::get('/add-product', ['as' => 'addProduct', 'uses' => 'ProductController@index'])->middleware('auth');
Route::post('/add-product', ['as' => 'addProduct', 'uses' => 'ProductController@store'])->middleware('auth');

